#!/bin/bash

set -e

export CI_COMMIT_SHA=minikube-sha
export APP_VERSION=minikube-app-version
export CONTAINER_RELEASE_IMAGE=bitzy:latest
export REPLICAS=1
export APP_PATH=/bitzy
export IMAGE_PULL_POLICY=IfNotPresent

eval $(minikube docker-env)

if [ "$1" = "build" ]; then 
    sbt compile universal:packageZipTarball
    docker build -t $CONTAINER_RELEASE_IMAGE .
fi;
envsubst < deploy/minikube/app.k8s.tmpl.yaml > deploy/minikube/.k8s.minikube.yaml

minikube addons enable ingress
kubectl apply -f deploy/minikube/.k8s.minikube.yaml

kubectl get ingress bitzy-api-ingress
