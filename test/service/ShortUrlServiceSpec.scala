package service

import org.scalatestplus.play._
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers.any
import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.{ ExecutionContext, Future }
import scala.collection.mutable
import scalaz.{ -\/, \/- }
import service.ShortUrlService
import dtos.ShortUrl
import org.joda.time.DateTime
import errors._
import dtos.CreateShortUrlRequest
import repository.ShortUrlRepository

/**
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class ShortUrlServiceSpec extends PlaySpec with MockitoSugar {

  implicit val executionContext = ExecutionContext.global

  def repository = mutable.HashMap[String, ShortUrl]()

  val req = CreateShortUrlRequest(Some("alias"), "http://url.com")

  "ShortUrlService" should {

    "create a new ShortUrl with or without an alias specified" in {
      val repository = mock[ShortUrlRepository]
      val service = new ShortUrlService(repository)
      when(repository.save(any[ShortUrl])).thenReturn(Future.successful(\/-(())))
      val createResponse = await(service.create(req))
      createResponse mustBe Symbol("right")

      val reqWithoutAlias = req.copy(alias = None)
      val createResponseWithoutAlias = await(service.create(reqWithoutAlias))
      createResponseWithoutAlias mustBe Symbol("right")
    }

    "respond with NotFound error when trying to get an non-existing alias" in {
      val repository = mock[ShortUrlRepository]
      val service = new ShortUrlService(repository)
      val alias = "alias-that-does-not-exist"
      when(repository.get("alias")).thenReturn(Future.successful(-\/(NotFoundError(""))))
      val getResponse = await(service.get("alias"))
      getResponse must matchPattern { case -\/(NotFoundError(_)) => }
    }

    // "create a new ShortUrl without an alias specified, and be able to retrieve it" in {
    //   val service = new ShortUrlService(repository)
    //   val reqWithoutAlias = req.copy(alias = None)

    //   val createResponse = await(service.create(reqWithoutAlias))
    //   createResponse mustBe Symbol("right")
    //   val created = createResponse.toOption.get

    //   val getResponse = await(service.get(created.alias))
    //   createResponse mustBe \/-(created)
    // }

    // "create a new ShortUrl and get a ConflictError when trying to create it again" in {
    //   val service = new ShortUrlService(repository)

    //   val createResponse = await(service.create(req))
    //   createResponse mustBe Symbol("right")

    //   val secondCreateResponse = await(service.create(req))
    //   secondCreateResponse must matchPattern { case -\/(ConflictError(_)) => }
    // }

    // "respond with NotFound error when trying to get an non-existing alias" in {
    //   val service = new ShortUrlService(repository)
    //   val getResponse = await(service.get("invalid-alias"))
    //   getResponse must matchPattern { case -\/(NotFoundError(_)) => }
    // }

  }

}
