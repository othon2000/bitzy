package controllers

import org.scalatestplus.play._
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito._
import play.api.test._
import play.api.test.Helpers._
import service.ShortUrlService
import scala.concurrent.{ ExecutionContext, Future }
import scalaz.{ -\/, \/- }
import dtos.ShortUrl
import org.joda.time.DateTime
import errors._
import dtos.CreateShortUrlRequest
import play.api.libs.json.Json
import generators.BitzyGenerators
import mappers.Mapper

/**
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class AdminControllerSpec extends PlaySpec with MockitoSugar with ControllersJsonSupport with BitzyGenerators {

  implicit val executionContext = ExecutionContext.global

  "AdminControllerSpec" should {

    "respond with recently created ShortUrl when creation is successful without specifying an alias" in {
      val shortUrlService = mock[ShortUrlService]
      val reqWithoutAlias = randomCreateShortUrlRequest.copy(alias = None)
      val shortUrl = Mapper.createShortUrlRequestToShortUrl(reqWithoutAlias)
      when(shortUrlService.create(reqWithoutAlias)).thenReturn(Future.successful(\/-(shortUrl)))

      val controller = new AdminController(stubControllerComponents(), shortUrlService)
      val response = controller.create().apply(FakeRequest().withBody(Json.toJson(reqWithoutAlias)))

      status(response) mustBe CREATED
      contentAsJson(response).as[ShortUrl] mustBe shortUrl
    }

    "respond with recently created ShortUrl when creation is successful with an alias" in {
      val shortUrlService = mock[ShortUrlService]
      val req = randomCreateShortUrlRequest
      val shortUrl = Mapper.createShortUrlRequestToShortUrl(req)
      when(shortUrlService.create(req)).thenReturn(Future.successful(\/-(shortUrl)))

      val controller = new AdminController(stubControllerComponents(), shortUrlService)
      val response = controller.create().apply(FakeRequest().withBody(Json.toJson(req)))

      status(response) mustBe CREATED
      contentAsJson(response).as[ShortUrl] mustBe shortUrl
    }

    "respond with HTTP 400 bad request if a bad JSON is sent" in {
      val shortUrlService = mock[ShortUrlService]

      val controller = new AdminController(stubControllerComponents(), shortUrlService)
      val response = controller.create().apply(FakeRequest().withBody(Json.toJson("{'a':'b'}")))

      status(response) mustBe BAD_REQUEST
    }

    "respond with HTTP 409 when creation fails because alias is already present" in {
      val shortUrlService = mock[ShortUrlService]
      val req = randomCreateShortUrlRequest
      when(shortUrlService.create(req)).thenReturn(Future.successful(-\/(ConflictError(""))))

      val controller = new AdminController(stubControllerComponents(), shortUrlService)
      val response = controller.create().apply(FakeRequest().withBody(Json.toJson(req)))

      status(response) mustBe CONFLICT
    }

    "respond with HTTP 500 when creation fails because of InternalServerError" in {
      val shortUrlService = mock[ShortUrlService]
      val req = randomCreateShortUrlRequest
      when(shortUrlService.create(req)).thenReturn(Future.successful(-\/(InternalServerError(""))))

      val controller = new AdminController(stubControllerComponents(), shortUrlService)
      val response = controller.create().apply(FakeRequest().withBody(Json.toJson(req)))

      status(response) mustBe INTERNAL_SERVER_ERROR
    }

    "respond with ShortUrl when fetching with an alias that exists" in {
      val shortUrlService = mock[ShortUrlService]
      val shortUrl = randomShortUrl
      when(shortUrlService.get(shortUrl.alias)).thenReturn(Future.successful(\/-(shortUrl)))

      val controller = new AdminController(stubControllerComponents(), shortUrlService)
      val response = controller.get(shortUrl.alias).apply(FakeRequest())

      status(response) mustBe OK
      contentAsJson(response).as[ShortUrl] mustBe shortUrl
    }

    "respond with HTTP 404 when fetching with an alias that does not exist" in {
      val shortUrlService = mock[ShortUrlService]
      val shortUrl = randomShortUrl
      when(shortUrlService.get(shortUrl.alias)).thenReturn(Future.successful(-\/(NotFoundError(""))))

      val controller = new AdminController(stubControllerComponents(), shortUrlService)
      val response = controller.get(shortUrl.alias).apply(FakeRequest())

      status(response) mustBe NOT_FOUND
    }

  }

}
