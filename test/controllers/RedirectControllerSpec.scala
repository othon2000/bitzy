package controllers

import org.scalatestplus.play._
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito._
import play.api.test._
import play.api.test.Helpers._
import service.ShortUrlService
import scala.concurrent.{ ExecutionContext, Future }
import scalaz.{ -\/, \/- }
import dtos.ShortUrl
import org.joda.time.DateTime
import errors._
import generators.BitzyGenerators

/**
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class RedirectControllerSpec extends PlaySpec with MockitoSugar with BitzyGenerators {

  implicit val executionContext = ExecutionContext.global

  "RedirectControllerSpec" should {

    "redirect to url when the alias is present in the service" in {
      val shortUrlService = mock[ShortUrlService]
      val shortUrl = randomShortUrl
      when(shortUrlService.get(shortUrl.alias)).thenReturn(Future.successful(\/-(shortUrl)))
      val controller = new RedirectController(stubControllerComponents(), shortUrlService)

      val redirect = controller.redirect(shortUrl.alias).apply(FakeRequest(GET, "/"))

      status(redirect) mustBe MOVED_PERMANENTLY
      redirectLocation(redirect) mustBe Some(shortUrl.url)
    }

    "return HTTP 404 when the alias is not present in the service" in {
      val shortUrlService = mock[ShortUrlService]
      val alias = randomAlias
      when(shortUrlService.get(alias)).thenReturn(Future.successful(-\/(NotFoundError(""))))
      val controller = new RedirectController(stubControllerComponents(), shortUrlService)

      val redirect = controller.redirect(alias).apply(FakeRequest(GET, "/"))

      status(redirect) mustBe NOT_FOUND
    }

    "return HTTP 500 when the service returns an InternalServerError" in {
      val shortUrlService = mock[ShortUrlService]
      val alias = randomAlias
      when(shortUrlService.get(alias)).thenReturn(Future.successful(-\/(InternalServerError())))
      val controller = new RedirectController(stubControllerComponents(), shortUrlService)

      val redirect = controller.redirect(alias).apply(FakeRequest(GET, "/"))

      status(redirect) mustBe INTERNAL_SERVER_ERROR
    }

  }

}
