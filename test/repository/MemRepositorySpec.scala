package repository

import org.scalatestplus.play._
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers.any
import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.{ ExecutionContext, Future }
import scalaz.{ -\/, \/- }
import dtos.ShortUrl
import org.joda.time.DateTime
import errors._
import generators.BitzyGenerators

/**
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class MemRepositorySpec extends PlaySpec with MockitoSugar with BitzyGenerators {

  implicit val executionContext = ExecutionContext.global

  "MemRepository" should {

    "create a new ShortUrl without an alias specified, and be able to retrieve it" in {
      val element = randomShortUrl
      val repository = new MemRepository()
      val saveResponse = await(repository.save(element))
      saveResponse mustBe \/-(())

      val getResponse = await(repository.get(element.alias))
      getResponse mustBe \/-(element)
    }

    "create a new ShortUrl and get a ConflictError when trying to create it again" in {
      val element = randomShortUrl
      val repository = new MemRepository()
      val saveResponse = await(repository.save(element))
      saveResponse mustBe \/-(())

      val secondCreateResponse = await(repository.save(element))
      secondCreateResponse must matchPattern { case -\/(ConflictError(_)) => }
    }

    "respond with NotFound error when trying to get an non-existing alias" in {
      val repository = new MemRepository()
      val getResponse = await(repository.get(randomAlias))
      getResponse must matchPattern { case -\/(NotFoundError(_)) => }
    }

  }

}
