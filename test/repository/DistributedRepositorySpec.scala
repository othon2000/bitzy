package repository

import org.scalatestplus.play._
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.ArgumentMatchers.any
import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.{ ExecutionContext, Future }
import scalaz.{ -\/, \/- }
import dtos.ShortUrl
import org.joda.time.DateTime
import errors._
import generators.BitzyGenerators
import play.api.Configuration
import org.scalacheck.Gen

/**
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class DistributedRepositorySpec extends PlaySpec with MockitoSugar with BitzyGenerators {

  implicit val executionContext = ExecutionContext.global

  val tolerance = 0.15

  "DistributedRepository" should {

    s"for a large random batch, distribute evenly (+- ${tolerance * 100}%) accross partitions" in {
      val config = mock[Configuration]
      val partititions = randomInteger(2, 20)
      when(config.getOptional[Int]("repository.nodes")).thenReturn(Some(partititions))
      val repository = new DistributedRepository(config)

      val totalNumber = randomInteger(100, 1000) * partititions
      for (_ <- 1 to totalNumber) await(repository.save(randomShortUrl)) mustBe \/-(())

      val qty = repository.sizes().sum
      qty mustBe totalNumber
      val average: Double = qty / partititions
      val toleranceRange = average +- (average * tolerance)
      repository.sizes() foreach (_.toDouble mustBe toleranceRange)
    }

    s"for a large random batch, make sure that every saved item can be retrieved" in {
      val config = mock[Configuration]
      val partititions = randomInteger(2, 20)
      when(config.getOptional[Int]("repository.nodes")).thenReturn(Some(partititions))
      val repository = new DistributedRepository(config)

      val totalNumber = randomInteger(100, 1000) * partititions
      for (_ <- 1 to totalNumber) {
        val element = randomShortUrl
        await(repository.save(element)) mustBe \/-(())
        await(repository.get(element.alias)) mustBe \/-((element))
      }
    }

  }

}
