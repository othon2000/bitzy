package generators

import org.scalacheck.{ Arbitrary, Gen }
import dtos.ShortUrl
import org.joda.time.DateTime
import dtos.CreateShortUrlRequest

trait BitzyGenerators {
  implicit lazy val shortUrlGenArb: Arbitrary[ShortUrl] = Arbitrary(BitzyGenerators.shortUrlGen)
  def randomAlias: String = BitzyGenerators.sample(BitzyGenerators.aliasGen)
  def randomShortUrl: ShortUrl = BitzyGenerators.sample(BitzyGenerators.shortUrlGen)
  def randomCreateShortUrlRequest: CreateShortUrlRequest = BitzyGenerators.sample(BitzyGenerators.createShortUrlRequestGen)
  def randomInteger(min: Int, max: Int): Int = BitzyGenerators.sample(Gen.choose(min, max))
}

private[generators] object BitzyGenerators {
  def sample[T](gen: Gen[T]): T = gen.sample match {
    case Some(value) => value
    case None        => sample(gen)
  }

  lazy val aliasGen: Gen[String] = Gen.alphaStr.suchThat(_.length > 10)
  lazy val urlGen: Gen[String] = Gen.alphaStr.suchThat(_.length > 50)
  lazy val dateTimeGen: Gen[DateTime] = Gen.posNum[Long].map(new DateTime(_).withMillisOfSecond(0))

  lazy val shortUrlGen: Gen[ShortUrl] = for {
    alias <- aliasGen
    url <- urlGen
    createdAt <- dateTimeGen
  } yield ShortUrl(alias, url, createdAt)

  lazy val createShortUrlRequestGen: Gen[CreateShortUrlRequest] = for {
    alias <- Gen.option(aliasGen)
    url <- urlGen
  } yield CreateShortUrlRequest(alias, url)
}
