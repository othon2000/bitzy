# Bitzy

URL shortener written with the [Scala Play framework](https://www.playframework.com/documentation/2.8.x/Home). 

## Build requirements

- JDK8+
- sbt 1.3.13 (easiest way to setup: [sdkman](https://sdkman.io/))

## Deploying

Deployment options (and instructions) are available in the [deploy](./deploy/) folder.

At the time of the writing, there is a running version on http://bitzy.xyz deployed to [GCP](./deploy/gcp/README.md) via [gitlab](./.gitlab-ci.yml).

## About the Persistence

The persistence layer simulates the behavior of a distributed partitioned database. Because this is currently running in memory, the application can only work effectively as single instance. The partitioning strategy is [properly tested](./test/repository/DistributedRepositorySpec.scala) to ensure even distribution.

There is a [WIP branch](https://gitlab.com/othon2000/bitzy/-/tree/mongodb) to work with MongoDB, but I had networking issues with Kubernetes and did not have enough time to finish (running locally works).

### Creating a new Short Url

```mermaid
graph TD
postReq("POST /admin {'url':'http://a.b'}")-->|1.|AdminController
AdminController-->|2.|ShortUrlService
ShortUrlService-->|3. generates alias and|save>"save(shortUrl)"]
subgraph "DistributedRepository"
  save-->|4. finds repo|repositoryOf>"repositoryOf(alias)"]
  save-->|5. saves the data|MemRepository1
  subgraph nodes
    MemRepository1-->store1
    MemRepository2-->store2
    MemRepositoryN-->storeN
  end
end
AdminController-->|6. HTTP 201|postReq
```

### Redirecting
```mermaid
graph TD
getReq("GET /{alias}")-->|1.|RedirectController
RedirectController-->|2.|ShortUrlService
ShortUrlService-->|3.|get>"get(alias)"]
subgraph "DistributedRepository"
  get-->|4. finds repo|repositoryOf>"repositoryOf(alias)"]
  get-->|5. retrieves the URL if available|MemRepository1
  subgraph nodes
    MemRepository1-->store1
    MemRepository2-->store2
    MemRepositoryN-->storeN    
  end
end
RedirectController-->|6. HTTP 301 or 404|getReq
```

## Commands

Running the application -- server will be brought up on http://localhost:9000
```sh
sbt run
```
Running tests
```sh
sbt test
```
Running tests with coverage report
```sh
sbt coverage test coverageReport (will be available at `target/scala-2.13/scoverage-report/index.html`)
```
Packaging and running with Docker -- server will be brought up on http://localhost:8080
```sh
sbt compile universal:packageZipTarball
docker build -t bitzy:latest .
docker run -p 8080:9000 bitzy:latest
```
