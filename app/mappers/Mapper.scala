package mappers

import dtos._
import org.joda.time.DateTime
import scala.util.Random
import scala.language.postfixOps

object Mapper {
  def createShortUrlRequestToShortUrl(request: CreateShortUrlRequest): ShortUrl = {
    ShortUrl(request.alias.getOrElse((Random.alphanumeric take 8 mkString)), request.url, DateTime.now.withMillisOfSecond(0))
  }
}
