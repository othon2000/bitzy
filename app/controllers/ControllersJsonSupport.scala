package controllers

import play.api.libs.json.Json
import dtos._
import play.api.libs.json.Writes
import org.joda.time.DateTime
import play.api.libs.json.{ JodaWrites, JodaReads }

trait ControllersJsonSupport {

  implicit val dateTimeWriter: Writes[DateTime] = JodaWrites.jodaDateWrites("yyyy-MM-dd HH:mm:ss")
  implicit val dateTimeJsReader = JodaReads.jodaDateReads("yyyy-MM-dd HH:mm:ss")

  implicit val createShortUrlRequestJsonFormat = Json.format[CreateShortUrlRequest]

  implicit val shortUrlJsonFormat = Json.format[ShortUrl]

}
