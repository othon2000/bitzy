package controllers

import javax.inject._
import scala.concurrent.{ ExecutionContext, Future }
import scalaz.{ \/, \/-, -\/ }
import play.api._
import play.api.mvc._
import play.api.libs.json.{ Json, JsValue, Reads }

import service.ShortUrlService
import dtos._
import errors._
import play.api.libs.json.Writes

@Singleton
class AdminController @Inject() (val controllerComponents: ControllerComponents, shortUrlService: ShortUrlService)(
  implicit
  val ec: ExecutionContext
) extends BaseController
  with ControllersJsonSupport {

  def create() =
    Action.async(parse.json) { implicit request: Request[JsValue] =>
      withBadRequestRecover[CreateShortUrlRequest](request.body) { createShortUrlRequest =>
        shortUrlService
          .create(createShortUrlRequest)
          .map(toResult(_, Created.apply(_)))
      }
    }

  def get(alias: String) =
    Action.async { implicit request: Request[AnyContent] =>
      shortUrlService.get(alias).map(toResult(_, Ok.apply(_)))
    }

  //utility functions below

  def withBadRequestRecover[T: Reads](json: JsValue)(successFunction: T => Future[Result]): Future[Result] = {
    json
      .validate[T]
      .map(successFunction)
      .recoverTotal(e => {
        Future.successful(BadRequestError(s"invalid json sent: ${json.toString}").toErrorResult)
      })
  }

  def toResult[T: Writes](disj: \/[BitzyError, T], result: (JsValue) => Result): Result =
    disj match {
      case \/-(success) => result(Json.toJson(success))
      case -\/(error)   => error.toErrorResult
    }

  def toResult[T](disj: \/[BitzyError, T], result: Result): Result = {
    toResultWith[T](disj, _ => result)
  }

  def toResultWith[T](disj: BitzyError \/ T, resultFunc: T => Result): Result =
    disj match {
      case \/-(success) => resultFunc(success)
      case -\/(error)   => error.toErrorResult
    }
}
