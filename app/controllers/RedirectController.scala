package controllers

import javax.inject._
import scala.concurrent.ExecutionContext
import scalaz.{ \/, \/-, -\/ }
import play.api._
import play.api.mvc._
import play.api.libs.json.{ Json, JsValue, Reads }

import service.ShortUrlService
import dtos._
import errors._

@Singleton
class RedirectController @Inject() (val controllerComponents: ControllerComponents, shortUrlService: ShortUrlService)(
  implicit
  val ec: ExecutionContext
) extends BaseController {

  def redirect(alias: String) =
    Action.async { implicit request: Request[AnyContent] =>
      shortUrlService.get(alias).map {
        case \/-(shortUrl)             => MovedPermanently(shortUrl.url)
        case -\/(error: NotFoundError) => NotFound
        case -\/(anyError)             => anyError.toErrorResult
      }
    }
}
