package repository

import javax.inject._
import scala.reflect.ClassTag
import dtos.ShortUrl
import scala.concurrent.Future
import scalaz.\/
import errors.BitzyError
import scala.concurrent.ExecutionContext
import play.api.Configuration

/**
 * A distributed repository consists of many repositories.
 * The "repository.nodes" property is used, defaults to 2.
 *
 * For the sake of simplicity, we are defaulting to `MemRepository`, but any implementation could be used (and parameterized).
 *
 * The DistributedRepository uses a hash function to determine to which repository an element belongs or should belong. We are
 * using the `alias` field because it is the field used for GET requests.
 *
 */
class DistributedRepository @Inject() (config: Configuration)(implicit val ec: ExecutionContext) extends ShortUrlRepository {

  val nodes = config.getOptional[Int]("repository.nodes").getOrElse(2)
  val repositories = List.fill(nodes)(new MemRepository())

  private def partitionKey(s: ShortUrl): String = s.alias
  private def partitionOf(a: String): Int = scala.math.abs(a.hashCode()) % repositories.size
  private def repositoryOf(s: ShortUrl): ShortUrlRepository = repositories(partitionOf(partitionKey(s)))
  private def repositoryOf(key: String): ShortUrlRepository = repositories(partitionOf(key))

  def save(s: ShortUrl): Future[BitzyError \/ Unit] = {
    repositoryOf(s).save(s)
  }

  def get(alias: String): Future[BitzyError \/ ShortUrl] = {
    repositoryOf(alias).get(alias)
  }

  def sizes(): Seq[Int] = repositories.map(_.size())
}
