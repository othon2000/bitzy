package repository

import dtos._
import scala.concurrent.Future
import scalaz.\/
import errors.BitzyError
import com.google.inject.ImplementedBy

@ImplementedBy(classOf[DistributedRepository])
trait ShortUrlRepository {
  def save(request: ShortUrl): Future[BitzyError \/ Unit]
  def get(alias: String): Future[BitzyError \/ ShortUrl]
}
