package repository

import scala.collection.mutable
import scalaz.{ \/-, \/, -\/ }
import scala.concurrent.{ ExecutionContext, Future }

import dtos.ShortUrl
import dtos.CreateShortUrlRequest
import mappers.Mapper
import errors._

/**
 * Simple repository implementation that stores the items in memory
 *
 */
class MemRepository(implicit val ec: ExecutionContext) extends ShortUrlRepository {

  val store = mutable.HashMap[String, ShortUrl]()

  def save(element: ShortUrl): Future[BitzyError \/ Unit] = {
    store.get(element.alias) match {
      case Some(url) => Future.successful(-\/(ConflictError(s"alias ${element.alias} already exists")))
      case None => {
        store.addOne((element.alias, element))
        Future.successful(\/-(()))
      }
    }
  }

  def get(alias: String): Future[BitzyError \/ ShortUrl] = {
    store.get(alias) match {
      case Some(url) => Future.successful(\/-(url))
      case None      => Future.successful(-\/(NotFoundError(s"alias $alias not found")))
    }
  }

  def size(): Int = store.size
}
