package errors

import play.api.mvc.{ Result, Results }
import java.util.UUID
import play.api.libs.json.JsValue
import play.api.http.Status
import play.api.libs.json.Format
import play.api.libs.json.Json
import play.api.libs.json.JsResult

trait BitzyError {
  def id: UUID = UUID.randomUUID()
  def errorCode: Int
  def errorMessage: String
  def toErrorResult: Result =
    Results.Status(errorCode)(messageToJson).as("application/json")
  def messageToJson: JsValue =
    Json.toJson(new BitzyProblem(this))(BitzyError.problemFormat)
}

case class BitzyProblem(
  `type`:   String,
  status:   Int,
  detail:   String,
  instance: Option[String]
) {
  def this(error: BitzyError) =
    this(
      s"http://httpstatuses.com/${error.errorCode}",
      error.errorCode,
      error.errorMessage,
      None
    )
}

object BitzyError {
  val InternalServerErrorMessage = "a server error occurred."

  implicit val problemFormat = new Format[BitzyProblem] {
    val base = Json.format[BitzyProblem]
    override def writes(o: BitzyProblem): JsValue = base.writes(o)

    override def reads(json: JsValue): JsResult[BitzyProblem] =
      base.reads(json)
  }
}

case class NotFoundError(errorMessage: String) extends BitzyError {
  override def errorCode: Int = Status.NOT_FOUND
}

case class BadRequestError(errorMessage: String) extends BitzyError {
  override def errorCode: Int = Status.BAD_REQUEST
}

case class ConflictError(errorMessage: String) extends BitzyError {
  override def errorCode: Int = Status.CONFLICT
}

case class InternalServerError(errorMessage: String = "an internal server error ocurred") extends BitzyError {
  override def errorCode: Int = Status.INTERNAL_SERVER_ERROR
}
