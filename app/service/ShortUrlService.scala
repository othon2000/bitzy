package service

import javax.inject._
import scalaz.{ \/-, \/, -\/ }
import scala.concurrent.{ ExecutionContext, Future }

import dtos._
import mappers.Mapper
import errors._
import repository.ShortUrlRepository

@Singleton
class ShortUrlService @Inject() (repository: ShortUrlRepository)(implicit val ec: ExecutionContext) {

  def create(request: CreateShortUrlRequest): Future[BitzyError \/ ShortUrl] = {
    val element = Mapper.createShortUrlRequestToShortUrl(request)
    repository.save(element).map {
      case \/-(_)   => \/-(element)
      case -\/(err) => -\/(err)
    }
  }

  def get(alias: String): Future[BitzyError \/ ShortUrl] = {
    repository.get(alias)
  }
}
