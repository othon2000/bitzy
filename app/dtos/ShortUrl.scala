package dtos

import org.joda.time.DateTime

case class ShortUrl(alias: String, url: String, createdAt: DateTime)

case class CreateShortUrlRequest(alias: Option[String], url: String)
