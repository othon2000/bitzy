#!/bin/bash

kubectl delete ingress bitzy-api-ingress
kubectl delete service bitzy-api-service
kubectl delete deployment bitzy-api-deployment
