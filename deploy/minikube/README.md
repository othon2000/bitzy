# Minikube deployment

Minikube is useful to test your application on Kubernetes without having to wait for pipelines to complete. Setup instructions can be found [here](https://minikube.sigs.k8s.io/docs/start/)

## Commands


```sh
# needs to run from the project's root folder because of Docker
./deploy/minikube/minikube-clean.sh 
./minikube-deploy.sh build
# verify the host and port with and access http://{host}/bitzy
kubectl get ingress bitzy-api-ingress
```

Note that, in between versions of the application, you need to run `./deploy/minikube/minikube-clean.sh` to be able to test the changes. This is because the script only tags the image with `:latest` to not pollute your local Docker installation.
