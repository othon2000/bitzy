# GCP k8s cluster  configuration

## Service account
create  `gitlab-ci` service account
### Roles
Not necessarily exactly these ones, but these work
- Kubernetes Engine Developer
- Storage Admin
### Key
You need to create a key for the service account and download the JSON

## CI settings in gitlab
In variables, add:
- `GCLOUD_PROJECT_ID` with the ID (name) of the gcp project, e.g. `bitzy-293308`
- `GCLOUD_SERVICE_KEY` with the JSON of the account key
- `GCLOUD_CLUSTER_NAME` k8s cluster name, e.g. `bitzy-dev`
- `GCLOUD_CLUSTER_ZONE` k8s cluster zone, e.g. `us-central1-c`

The variables will be used as part of the pipeline

## Connecting to the cluster from your laptop
Requires [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and [gcloud sdk](https://cloud.google.com/sdk/docs/quickstart).

Download the same service key, or create a new one for yourself and save it to `~/gcloud-service-key.json`.

```sh
# set the variables (may need to change accordingly)
GCLOUD_PROJECT_ID=bitzy-293308
GCLOUD_CLUSTER_NAME=bitzy-dev
GCLOUD_CLUSTER_ZONE=us-central1-c
gcloud auth activate-service-account --key-file ~/gcloud-service-key.json
gcloud config set project $GCLOUD_PROJECT_ID
# will write k8s settings to ~/.kube/config
gcloud container clusters get-credentials $GCLOUD_CLUSTER_NAME --zone $GCLOUD_CLUSTER_ZONE
# verify
kubectl get po -A
```

### Finding the IP address
```sh
kubectl get svc
# output example, get the "CLUSTER-IP" column
# NAME                TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)        AGE
# bitzy-api-service   LoadBalancer   10.4.14.241   35.224.63.97   80:31918/TCP   30m
```
