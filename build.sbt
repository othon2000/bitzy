name := """bitzy"""
organization := "com.othoncrelier.experiments.bitzy"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.3"

libraryDependencies += guice
libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.3.2"
libraryDependencies += "com.typesafe.play" %% "play-json-joda" % "2.8.1"

libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
libraryDependencies += "org.mockito" %% "mockito-scala" % "1.14.8" % Test
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.1" % Test

coverageExcludedPackages := "<empty>;Reverse.*;router.*"
