FROM openjdk:8

ARG SERVICE_NAME="bitzy"
ARG SERVICE_VERSION="1.0"

ADD "target/universal/${SERVICE_NAME}-${SERVICE_VERSION}.tgz" /opt/docker/

RUN chmod +x "/opt/docker/${SERVICE_NAME}-${SERVICE_VERSION}/bin/${SERVICE_NAME}"
WORKDIR "/opt/docker/${SERVICE_NAME}-${SERVICE_VERSION}"

EXPOSE 9000

RUN echo "#!/bin/bash \n bin/${SERVICE_NAME}" > ./run_me.sh
RUN chmod +x ./run_me.sh

ENTRYPOINT ["./run_me.sh"]
CMD []
